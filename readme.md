# Devcentralised Meetup
![Meetup Devcentralised](https://gitlab.com/caelumlabs/devcentralised/raw/master/meetupdevcentralised_header.png "")  
Devcentralised is a group for those who are interested in developing decentralised apps or just learning about ethereum, hyperledger and other blockchain technologies.
We want to meet other DApps developers in the Barcelona to organise presentations, coding dojos, hack days, workshops, etc. focused on decentralised applications.

[Click here](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/) to join the Meetup group. [Follow us on Twitter](https://twitter.com/caelumlabs) and join our Newsletter [here](http://newsletter.caelumlabs.com).

## PAST MEETUPS

### 20/11/2018 - The sharing economy without intermediaries is possible with Origin Protocol 

**Participants**: Leonardo Bautista [Professional page](https://www.bsc.es/bautista-gomez-leonardo)  
**Resources**:  [Slides](https://gitlab.com/caelumlabs/devcentralised/raw/master/slides/20112018_devcentralised_sharding.pdf?inline=false) - [Mini video] - [Video]  
[**Link**](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/255962360/)

### 13/11/2018 - The sharing economy without intermediaries is possible with Origin Protocol 

**Participants**: Jon Hearty [@jonhearty](https://twitter.com/jonhearty)  
**Resources**:  [Slides](https://gitlab.com/caelumlabs/devcentralised/raw/master/slides/13112018_origin_devcentralised.pdf?inline=false) - [Mini video] - [Video]  
[**Link**](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/256220632/)


### 08/11/2018 - Discover Raiden, a lightning payment solution for Ethereum

**Participants**: Jacob S. Czepluch [@_czepluch](https://twitter.com/_czepluch) & Dominik Schmid [Linkedin](https://www.linkedin.com/in/dominik-schmid-580a3b82/)  
**Resources**:  
[**Link** 
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/255964749/)  


### 05/06/2018 - Code your own Nation on the blockchain : Bitnation Χ Devcentralised

**Participants**: Erik Vollstädt [Linkedin](https://www.linkedin.com/in/erikvollstaedt)  
**Resources**: [Mini video] - [Video](https://www.youtube.com/watch?v=XTQTyWnO7fE)  
[**Link** 
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/251198665/)  

### 15/02/2018 - A Self-Sovereign Identity Standard For Ethereum

**Participants**: Fabian Vogelsteller [@feindura](https://twitter.com/feindura)  
**Resources**:  
[**Link** 
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/247340544/)

### 15/02/2018 - A Self-Sovereign Identity Standard For Ethereum

**Participants**: Fabian Vogelsteller [@feindura](https://twitter.com/feindura) & Jose Luis Muñoz [Personal page](https://futur.upc.edu/JoseLuisMunozTapia) & Alex Puig [@alexpuig](https://twitter.com/alexpuig)  
**Resources**:  
[**Link**  
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/247340544/)  

### 13/12/2017 - What you need to know about uPort identities as a developer

**Participants**: @ajunge_m [@ajunge_m](https://twitter.com/ajunge_m)  
**Resources**:  
[**Link**  
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/245640984/)  

### 10/10/2017 - Solidity by Example: ERC20 & uPort

**Participants**: Alex Puig [@alexpuig](https://twitter.com/alexpuig)  
**Resources**:  
[**Link**  
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/243548033/)  

### 20/09/2017 - Blockchain 101

**Participants**: Alex Puig [@alexpuig](https://twitter.com/alexpuig)  
**Resources**:  
[**Link**  
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/243356456/)  

### 20/09/2017 - Ethereum ecosystem : uPort, Whisper, IPFS & ENS

**Participants**: Alex Puig [@alexpuig](https://twitter.com/alexpuig)  
**Resources**:  
[**Link**  
](https://www.meetup.com/Devcentralised-Developing-DApps-Barcelona/events/242462495/)  
